= About
:showtitle:
:page-title: About
:page-description: About me, what I am interested in, and what I am not interested in.

This page tells you a little more about me (go link:cv.html[here] if you want a formal CV) and what I am interested in, and just as importantly, what I am not interested in.

== What I am interested in

I like coming up with answers to hard questions, such as:

* How can companies make sure that operational technology systems (airports, seaports, factories, etc.) receive *at least* the same amount of cybersecurity protections as information technology systems?
* How can a manufacturer substantively reduce the risks and uncertainty associated with importing raw materials in a tough geopolitical climate without passing on significant cost increases to their customers?
* How can a state make sure that individuals who live in rural and underprivileged areas have cost-effective access to STEM education *of the same quality* as those who live in wealthy urban and suburban areas?
* How can large, bureaucratic organization adapt and apply agile development methodologies to every day business operations, even those that have nothing to do with writing code?
* How can a state cost-effectively deliver terrestrial broadband access to its residents regardless of their address?

I've led efforts in each of those areas and have broad experience working with state, federal, private, and academic entities in a collaborative environment.
You can read about some of them in my CV and various media if you Google around a bit, but if you'd like to know more, ask away!

== What I am not interested in

I spent a majority of my professional life up to this point inside the Defense Industrial Base (DIB) in one form or another.
That has greatly influenced the work I am interested in at this point in my career.
I also don't have a whole lot of time available for work, so I want to make the most of it.

I do not wish to partake in opportunities that:

* Requires a security clearance of any kind.
Public Trust (and things in that vein) are OK.
* Works directly with Department of Defense entities (with a potential exception for a Service Academy doing research, depending on the research).
* Works with an entity that makes a majority of their revenue from Department of Defense contracts.
If the entity is a conglomerate and there are separate and distinct commercial and DoD/public sector legal entities, that is OK.
* Are considered a commodity.  Generic cybersecurity efforts, system administration tasks (even management and strategy roles) are included here.

== My rates

$250 USD/hour.

If you have an _exceptionally_ interesting problem, a reliable funding source that allows for rapid iteration, a strong team already in place, and can guarantee at least 400 hours of work within a given 12-month period, we can discuss reducing that rate in a manner commensurate with the degree that the aforementioned requirements are met.