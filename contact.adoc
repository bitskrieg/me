= Contact
:showtitle:
:page-title: Contact
:page-description: Contact info, github, social media, etc.

* http://github.com/bitskri3g[Github]
* http://gitlab.com/bitskrieg[Gitlab]
* http://bitskrieg.io[Website]

If you have a problem that you think I would be interested in, you can reach me mailto:bitskrieg@bitskrieg.net[here].